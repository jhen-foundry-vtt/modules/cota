//#region imports
//#endregion    

/**
 * @class Repeaters
 * @classdesc Cette classe est une liste de répétiteur
 * @extends {Array}
 * @author: jhenriot
 * 
 * @property {Array<Repeater>}
 * 
 * @method onRepetearsActions : Abonnement des répétiteurs selon les actions Ajout et Suppression
 * 
 */
export class Repeaters extends Array {
	//#region propriétés
	static get [Symbol.species]() { return Array; }
	//#endregion

	//#region méthodes publiques
	/**
	   * @name onRepetearsActions
	   * @description : Abonnement des répétiteurs selon les actions Ajout et Suppression
	   * 
	   * @param {ItemSheet} sheet -> l'instance de la feuille ou se trouve la liste de répétiteurs
	   * @param {Event} event -> l'évènement associé [ajouter] ou [supprimer]
	   * 
	   * @public
	   */
	onRepetearsActions(sheet, event) { this.forEach(repeater => { this._repeaterActions(sheet, repeater, event); }); }
	//#endregion

	//#region méthodes privées
	/**
	 * @name _repeaterActions
	 * @description Gestion des actions [ajouter] et [supprimer] d'un répétiteur
	 * 
	 * @param {ItemSheet} sheet la fiche 
	 * @param {Repeater} repeater le répétiteur
	 * @param {Event} event l'évènement associé [ajouter] ou [supprimer]
	 * 
	 * @private @async
	 */
	async _repeaterActions(sheet, repeater, event) {
		event.preventDefault();
		const a = event.currentTarget;
		// ajoute un nouveau répétiteur
		if (a.classList.contains("ajouter-" + repeater.classe)) {
			await sheet._onSubmit(event);
			const dataRepeteur = repeater.data;
			if (dataRepeteur !== undefined) {
				return await sheet.object.update(JSON.parse("{ \"" + repeater.dataString + "\":" + JSON.stringify(dataRepeteur.concat([repeater.dataEmpty])) + "}"));
			}
		}
		// supprime un répétiteur
		if (a.classList.contains("enlever-" + repeater.classe)) {
			await sheet._onSubmit(event);
			const li = a.closest(".repeater");
			const dataRepeteur = foundry.utils.duplicate(repeater.data);
			dataRepeteur.splice(Number(li.dataset.repeater), 1);
			return await sheet.object.update(JSON.parse("{ \"" + repeater.dataString + "\":" + JSON.stringify(dataRepeteur) + "}"));
		}
	}
	//#endregion
};

/**
 * @class Repeater
 * @classdesc Cette classe modèle représente un répétiteur
 * @author: jhenriot
 */
export class Repeater {
	//#region propriétés
	/**
	 * 
	 */
	#classe = null;
	get classe() { return this.#classe; }
	set classe(value) { this.#classe = value; }
	/**
	 * 
	 */
	#data = null;
	get data() { return this.#data; }
	set data(value) { this.#data = value; }
	/**
	 * 
	 */
	#dataString = null;
	get dataString() { return this.#dataString; }
	set dataString(value) { this.#dataString = value; }
	/**
	 * 
	 */
	#dataEmpty = null;
	get dataEmpty() { return this.#dataEmpty; }
	set dataEmpty(value) { this.#dataEmpty = value; }
	//#endregion

	//#region méthodes publiques
	/**
	 * 
	 * @param {String} classe 
	 * @param {JSON} data 
	 * @param {String} dataString 
	 * @param {Array} dataEmpty 
	 */
	constructor(classe, data, dataString, dataEmpty) {
		this.#classe = classe;
		this.#data = data;
		this.#dataString = dataString;
		this.#dataEmpty = dataEmpty;
	}
	/**
	 * 
	 * @param {item} formData les données du formulaire
	 */
	update(formData) {
		let data = Object.entries(formData).filter(e => e[0].startsWith(this.dataString));
		formData[this.dataString] = data.reduce((arr, entry) => {
			let [i, j] = entry[0].split(".").slice(entry[0].split(".").length - 2);
			if (!arr[i]) arr[i] = [];
			arr[i][j] = entry[1];
			return arr;
		}, []);
	}
	//#endregion
};
