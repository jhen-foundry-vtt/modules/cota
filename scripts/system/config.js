export const COTA = {};

COTA.martialTrainingCategory = {
	"farmerWeapons": "COTA.profile.farmerWeapons.label",
	"warWeapons": "COTA.profile.warWeapons.label",
	"heavyWarWeapons": "COTA.profile.heavyWarWeapons.label",
	"hastWeapons": "COTA.profile.hastWeapons.label",
	"duelingWeapons": "COTA.profile.duelingWeapons.label",
	"throwingWeapons": "COTA.profile.throwingWeapons.label",
	"draughtWeapons": "COTA.profile.draughtWeapons.label",
	"shootingWeapons": "COTA.profile.shootingWeapons.label",
	"lightArmour": "COTA.profile.lightArmour.label",
	"heavyArmour": "COTA.profile.heavyArmour.label"
};
COTA.paths = [];
