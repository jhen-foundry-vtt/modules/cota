export const registerHandlebarsHelpers = function () {

	Handlebars.registerHelper('isWeaponOrArmor', function (category) {
		if (category === "armor" || category === "shield" || category === "melee" || category === "ranged") return true;
		return false;
	});
	Handlebars.registerHelper('select', function (selected, options) { 
		const escapedValue = RegExp.escape(Handlebars.escapeExpression(selected));
		const rgx = new RegExp(' value=[\"\']' + escapedValue + '[\"\']');
		const html = options.fn(this);
		return html.replace(rgx, "$& selected");	
	});
}