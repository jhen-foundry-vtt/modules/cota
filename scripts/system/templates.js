/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function () {

	// Define template paths to load
	const templatePaths = [
		// ACTOR
		"modules/cota/templates/actors/parts/actor-tabs.hbs",        
		"modules/cota/templates/actors/parts/stats/actor-stats.hbs",
		"modules/cota/templates/actors/parts/stats/actor-defence.hbs",
		"modules/cota/templates/actors/parts/capacities/actor-paths.hbs",
		"modules/cota/templates/actors/parts/capacities/actor-capacities.hbs",
			   
		// DIALOGS
		'modules/cota/templates/dialogs/scalable-capacities-dialog.hbs',
		// ITEMS PROPERTIES

		// ITEMS DETAILS
		"modules/cota/templates/items/item-sheet.hbs",
		"modules/cota/templates/items/parts/details/capacity-details.hbs",
		"modules/cota/templates/items/parts/details/path-details.hbs",
		"modules/cota/templates/items/parts/details/profile-details.hbs",
		"modules/cota/templates/items/parts/details/item-details.hbs",
		"modules/cota/templates/items/parts/details/weapon-details.hbs"
	];

	// Load the template parts
	return loadTemplates(templatePaths);
};
