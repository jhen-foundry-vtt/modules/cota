/**
 * 
 */
export const registerSystemSettings = function() {
	game.settings.register("cof", "useIncompetentPJ", {
		name: "PJ incompétent",
		hint: "Utiliser la règle du PJ Incompétent (Livre du joueur p 105 et 157).",
		scope: "world",
		config: true,
		default: true,
		type: Boolean
	});

};
