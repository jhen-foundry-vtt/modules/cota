//#region imports
import { CofActorSheet } from "../../../../systems/cof/module/actors/actor-sheet.js";
import { Species } from "../../../../systems/cof/module/controllers/species.js";
import { CotaCapacity } from "../controllers/capacity.js";
import { CotaProfile } from "../controllers/profile.js";
import { CotaItem } from "../items/item.js";
import { CotaActor } from "./actor.js";
//#endregion

/**
 * @class CotaActorSheet
 * @classdesc classe contrôleur du formulaire des acteurs. Surcharge de la classe CofActorSheet pour les spécificités de COTA
 * @extends {CofActorSheet}
 * 
 * @property defaultOptions
 * @property template
 * 
 * @method getPackPrefix        
 * @method getPathRoot
 * @method getLogoPath
 * @method getCategory  : Obtenir le nom de la catégorie martiale de l'élément arme ou armure   
 * @method getData      : Surcharge permettant d'ajouter le regroupement sur les capacités évolutives
 * 
 */
export class CotaActorSheet extends CofActorSheet {

	//#region propriétés
	/** @override */
	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
			classes: ["cof", "cota", "sheet", "actor"],
			width: 970,
			height: 745
		});
	}
	/**
	 * @override
	 */
	get template() { return "modules/cota/templates/actors/actor-sheet.hbs"; }
	//#endregion

	//#region méthodes publiques
	/**
	 * @override
	 */
	getPackPrefix() { return "cota"; }
	/**
	 * @override
	 */
	getPathRoot() { return "modules/cota"; }
	/**
	 * @override
	 */
	getLogoPath() { return "/ui/logo-terres-arran-big.webp"; }
	/**
	 * @name getCategory
	 * @description Obtenir le nom de la catégorie martiale de l'élément arme ou armure
	 * @public @override
	 * 
	 * @param {CotaItem}  itemData les données de l'arme ou de l'armure
	 * 
	 * @returns {string} le nom de la catégorie martiale
	 */
	getCategory(itemData) { return itemData.moduleData?.cota?.martialCategory; }
	/** 
	 * @name getData
	 * @description Surcharge permettant d'ajouter le regroupement sur les capacités évolutives
	 * @public @override 
	 * 
	 * @param {Object} options
	 * @returns {CotaActor} les données de l'acteur
	 */
	async getData(options = {}) {
		this.actor.checkItemsForInsufficientStrength();
		const context = await super.getData(options);
		const collections = context.capacities.collections;
		collections.splice(collections.findIndex(item => item.id === "standalone-capacities"), 1);
		collections.push({
			id: "standalone-capacities",
			label: "Capacités Hors-Voies",
			items: Object.values(context.items).filter(item => { if (item.type === "capacity" && !item.system?.path && !item.system?.moduleData?.cota?.scalable) return true; })
		});
		collections.push({
			id: "scalable-capacities",
			label: "Capacités évolutives",
			items: Object.values(context.items).filter(item => { if (item.type === "capacity" && item.system?.moduleData?.cota?.scalable) return true; })
		});
		collections.sort((a, b) => (a.name > b.name) ? 1 : -1);
		return context;
	}
	//#endregion

	//#region méthodes privées
	/**
	 * @name _onCheckedCapacity
	 * @description implémentation lors de la sélection d'une capacité
	 * @private @override
	 * 
	 * @param {CotaActor} actor l'acteur
	 * @param {Event} event l'évènement
	 * @param {boolean} isUncheck la capcité est décochée
	 */
	_onCheckedCapacity(actor, event, isUncheck) { 
		const elt = $(event.currentTarget).parents(".capacity");
		// get id of clicked capacity
		const capId = elt.data("itemId");
		// get id of parent path
		const pathId = elt.data("pathId");
		return CotaCapacity.toggleCheck(actor, capId, pathId, isUncheck); 
	}
	/**
	 * @name _onDropItem
	 * @description surcharge pour prendre en compte : 
	 *                  - l'ajout du talent magique des profils mystiques, 
	 *                  - l'ajout du peuple privilégié si non renseigné
	 * @private @override @async
	 * 
	 * @param {Event} event l'évènement 
	 * @param {CotaItem} data les données d'un élément
	 */
	async _onDropItem(event, data) {
		const itemData = foundry.utils.duplicate(await Item.fromDropData(data));
		return await super._onDropItem(event, data).then(() => {
			if (itemData?.type === "profile") {
				if (!this.actor.items.find(item => item.type === "species")) {
					const id = itemData?.system.moduleData.cota.privilegedSpecie[0]._id;
					game.packs.get("cota.species").getDocument(id).then(specie => {
						specie._source.flags.core = { sourceId: "Compendium.cota.species." + specie.id };
						return Species.addToActor(this.actor, specie);
					});
				}
				return CotaProfile.addMagicTalendToActor(this.actor, itemData?.system?.moduleData.cota.magicTalent);
			}
		});
	}
	/**
	 * @name _onDeleteItem
	 * @description surcharge pour la suppression du talent magique du profil mystique
	 * @private @override
	 * 
	 * @param {Event} event l'évènement 
	 * 
	 * @returns {CotaItem} return l'élément supprimé
	 */
	_onDeleteItem(event) {
		try {
			return super._onDeleteItem(event).then(item => {
				if (item) {
					item = item[0];
					if (item && item.type === "profile") return CotaProfile.removeMagicTalendFromActor(this.actor, item.system?.moduleData?.cota?.magicTalent);
					else if (item && item.type === "capacity" && item.system?.scalable) return CotaCapacity.removeFromActor(this.actor, item);
					return CotaCapacity.checkAdvancedCapacity(this.actor, item.system?.key);
				}
			});
		} catch (error) { console.log(error); }
	}
	//#endregion
}
