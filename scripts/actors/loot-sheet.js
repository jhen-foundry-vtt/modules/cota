//#region imports
import { CofLootSheet } from "../../../../systems/cof/module/actors/loot-sheet.js";
//#endregion

/**
 * @class CotaLootSheet
 * @classdesc classe contrôleur du formulaire des butins. Surcharge de la classe CofLootSheet pour les spécificités de COTA
 * @extends {CofLootSheet}
 * 
 * @property defaultOptions
 * 
 * @method getPackPrefix        
 * @method getPathRoot
 * @method getLogoPath
 * 
 */
 export class CotaLootSheet extends CofLootSheet {
	//#region propriétés
	/** @override */
	static get defaultOptions() { return foundry.utils.mergeObject(super.defaultOptions, { classes: ["cof", "cota", "sheet", "actor"], }); }
	//#endregion

	//#region méthodes publiques
	/**
	 * @override
	*/
	getPackPrefix() { return "cota"; }

	/**
	 * @override
	 */
	getPathRoot() { return "modules/cota"; }
	/**
	* @override
	*/
	getLogoPath() { return "/ui/logo-terres-arran-big.webp"; }
	//#endregion
}
