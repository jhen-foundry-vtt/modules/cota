//#region import
import { CofActor } from "../../../../systems/cof/module/actors/actor.js";
//#endregion

/**
 * @class CotaActor
 * @classdesc  Cette classe étend l'entité acteur de Chroniques Oubliées Fantaisy pour prendre en compte les modifications 
 *             du module Chroniques Oubliées - Les Terres d'Arran. Les modifications portent sur le calcul des points de chance, 
 *             des points de récupération, et des attaques de mélée, à distance et magique selon la famille du profil.		      
 * @extends {CofActor}
 * 
 * @method computeBaseFP                    : Retourne le nombre de points de chance en fonction du charisme et du profil
 * @method computeBaseRP                    : Retourne 6 si c'est un ogre (déterminé par la capacité Endurant), sinon 5
 * @method getMeleeMod                      : Retourne le modificateur de force + le bonus de profil
 * @method getRangedMod                     : Retourne le modificateur de dextérité + le bonus de profil
 * @method getMagicMod                      : Retourne le modificateur d'intelligence + le bonus de profil
 * @method getMagicPoints                   : Calcule le nombre de points de magie en fonction du profil. Pour les profils Mystique, PM est multiplié par 2
 * @method getIncompetentArmour             : Obtenir la liste des armures non maîtrisées
 * @method getIncompetentShields            : Obtenir la liste des boucliers non maîtrisés
 * @method isCompetent                      : Vérifie si le profil est compétent pour la catégorie martiale
 * @method getIncompetentMalusToInitiative : Retourne le malus à l'initiative lié à l'armure et à l'incompétence armes/armures
 * @method getIncompetentArmourMalus        : Retourne le malus d'incompétence lié à l'armure 
 * @method getIncompetentShieldMalus        : Retourne le malus d'incompétence lié au bouclier
 * @method getOverloadMalusToInitiative     : Retourne le malus d'encombrement à appliquer à l'initiative
 * @method getOverloadMalusToRangedAttack   : Retourne le malus d'encombrement à appliquer à l'attaque à distance
 * @method getOverloadMalusToMagicAttack    : Retourne le malus d'encombrement à appliquer à l'attaque magique
 * @method getOverloadedOtherMod            : Obtenir les modificateurs liés à l'encombrement d'autres sources pour l'initiative, l'attaque magique et l'attaque à distance
 * @method getActiveCapacities              : Obtenir la liste des capacités actives
 * @method canEquipItem                     : 
 * @method checkItemsForInsufficientStrength: vérifie que les armes dont la force minimum requise n'est pas respecté, soit désactivées
 * 
 */
export class CotaActor extends CofActor {
   
	//#region méthodes publiques
	/**
	 * @name computeBaseFP
	 * @description Retourne le nombre de points de chance en fonction du charisme et du profil
	 *              PC = 2 + Mod. de CHA  Les profils de la famille des aventuriers bénéficie d’un bonus supplémentaire de +2.
	 * @public @override
	 * 
	 * @param {Int} charismeMod le mod de charisme
	 * @param {CotaItem} profile le profil
	 * 
	 * @returns {Int} les points de chance
	 */
	computeBaseFP(charismeMod, profile) {
		let bonuses = null;
		if (profile) bonuses = profile.system.moduleData?.cota?.bonuses;
		return 2 + charismeMod + ((bonuses) ? bonuses.fp : 0);
	}
	/**
	 * @name computeBaseRP
	 * @description Retourne 6 si c'est un ogre (déterminé par la capacité Endurant), sinon 5
	 * @public @override
	 * 
	 * @param {CotaActor} actorData les données de l'acteur
	 * 
	 * @returns {Int} les points de récupération
	 */
	computeBaseRP(actorData) { return (actorData.items.find(item => item.system.key === "endurant")) ? 6 : 5; }
	/**
	 * @name getMeleeMod
	 * @description Retourne le modificateur de force + le bonus de profil
	 * @public @override
	 * 
	 * @param {object} stats les statistiques
	 * @param {CotaItem} profile le profil
	 */
	getMeleeMod(stats, profile) { return stats?.str.mod + ((profile?.system.moduleData?.cota?.bonuses) ? profile?.system.moduleData?.cota?.bonuses.melee : 0); }
	/**
	 * @name getRangedMod
	 * @description Retourne le modificateur de dextérité + le bonus de profil
	 * @public @override
	 * 
	 * @param {object} stats les statistiques 
	 * @param {CotaItem} profile le profil
	 */
	getRangedMod(stats, profile) { return stats?.dex.mod + ((profile?.system.moduleData?.cota?.bonuses) ? profile?.system.moduleData?.cota?.bonuses.ranged : 0); }
	/**
	 * @name getMagicMod
	 * @description Retourne le modificateur d'intelligence + le bonus de profil
	 * @public @override
	 * 
	 * @param {object} stats les statistiques
	 * @param {CotaItem} profile le profil
	 */
	getMagicMod(stats, profile) { return stats?.int.mod + ((profile?.system.moduleData?.cota?.bonuses) ? profile?.system.moduleData?.cota?.bonuses.magic : 0); }
	/**
	  * @name getMagicPoints
	  * @description Calcule le nombre de points de magie en fonction du profil. Pour les profils Mystique, PM est multiplié par 2
	  *              COTA : PM = Niveau + Mod SAG
	  *              
	  * @public @override
	  *
	  * @param {Int} level le niveau
	  * @param {object} stats les statistiques
	  * @param {CotaItem} profile le profil
	  * 
	  * @returns {Int} le nombre de points de magie
	  */
	getMagicPoints(level, stats, profile) {
		let pm = (profile && profile.system.mpfactor === "2") ? (level + stats?.wis.mod) * 2 : level + stats?.wis.mod;
		return (pm < 0) ? 0 : pm;
	}
	/**
	 * @name getIncompetentArmour
	 * @description obtenir la liste des armures non maîtrisées
	 * @public @override
	 * 
	 * @returns {Array} la liste des armures non maîtrisées
	 */
	getIncompetentArmour() { return this._getIncompetentItems("armor"); }
	/**
	 * @name getIncompetentShields
	 * @description obtenir la liste des boucliers non maîtrisés
	 * @public @override
	 * 
	 * @returns {Array} la liste des boucliers non maîtrisés
	 */
	getIncompetentShields() { return this._getIncompetentItems("shield"); }
	/**
	 * @name isCompetent
	 * @description Vérifie si le profil est compétent pour la catégorie martiale
	 * @public @override
	 * 
	 * @param martialCategory la catégorie martiale
	 * @param profile le profil
	 * 
	 * @returns {boolean} est compétent ou incompétent
	 */
	isCompetent(martialCategory, profile) { return (profile?.system.moduleData?.cota?.martialTraining[martialCategory]) ? true : false; }
	/**
	 * @name getOverloadMalusToInitiative
	 * @description Retourne le malus à l'initiative lié à l'armure
	 * @public @override
	 * 
	 * @returns {int} retourne le malus (négatif) ou 0
	 */
	getOverloadMalusToInitiative() {
		if (!game.settings.get("cof", "useOverload")) return 0;
		let malus = 0;
		// Encombrement de l'armure
		malus += this.getOverloadedMalusTotal();				
		// Incompétence avec l'armure
		malus += this.getIncompetentArmourMalus();
		// Incompétence avec le bouclier
		malus += this.getIncompetentShieldMalus();
		return malus;
	}
	/**
	 * @name getIncompententArmourMalus
	 * @description Obtenir le malus d'incompétence lié à l'armure
	 *              Nombre d'armures non maitrisées * le malus d'incompétence (-3)
	 * @public @override
	 * 
	 * @returns le malus d'armure ou 0
	 */
	getIncompetentArmourMalus() { return (!game.settings.get("cof", "useIncompetentPJ")) ? 0 : this.getIncompetentArmour().length * this.getIncompetentMalus(); }
	/**
	 * @name getIncompetentShieldMalus
	 * @description Obtenir  le malus d'incompétence lié au bouclier
	 *              Nombre de boucliers non maitrisées * le malus d'incompétence (-3)
	 * @public @override
	 * 
	 * @returns le malus de bouclier ou 0
	 */
	 getIncompetentShieldMalus() { return (!game.settings.get("cof", "useIncompetentPJ")) ? 0 : this.getIncompetentShields().length * this.getIncompetentMalus(); }
	/**
	 * @name getOverloadMalusToRangedAttack
	 * @description Retourne le malus à l'attaque à distance lié à l'encombrement et l'incompétence
	 * @public @override
	 * 
	 * @returns {int} retourne le malus (négatif) ou 0
	 */
	getOverloadMalusToRangedAttack() { return (!game.settings.get("cof", "useOverload")) ? 0 : -(Math.floor(Math.abs(this.getOverloadedMalusTotal() / 2))); }
	/**
	 * @name getOverloadMalusToMagicAttack
	 * @description Retourne le malus à l'attaque magique lié à l'encombrement et l'incompétence
	 * @public @override
	 * 
	 * @returns {int} retourne le malus (négatif) ou 0
	 */    
	getOverloadMalusToMagicAttack() { return (!game.settings.get("cof", "useOverload")) ? 0 : this.getOverloadedMalusTotal(); }
	/**
	 * @name getOverloadedOtherMod
	 * @description obtenir les modificateurs liés à l'encombrement d'autres sources pour l'initiative, l'attaque magique et l'attaque à distance
	 *      COF : Pas de malus
	 * 
	 * @returns {int} retourne le modificateur 
	 */
	getOverloadedOtherMod() { return this?.system?.attributes?.overload?.misc ? this?.system?.attributes?.overload?.misc : 0; } 
	/**
	 * @name getMalusFromArmor
	 * @description calcule le malus lié à l'armure équipée, égal à la DEF
	 * @public @override
	 * 
	 * @returns {Int} 0 ou la valeur négative du malus
	 */
	 getMalusFromArmor() {
		let malus = 0;
		let protections = this?.items.filter(i => i.type === "item" && i.system.subtype === "armor" && i.system.worn && i.system.def).map(i => (-1 * i.system.defBase));     
		if (protections.length > 0) malus = protections.reduce((acc, curr) => acc + curr, 0);
		return malus;
	}
	/**
	 * @name getActiveCapacities
	 * @description obtenir la liste des capacités actives
	 * @public @override
	 * @param {Array} items la liste des items de l'acteur
	 * 
	 * @returns la liste des capacités actives
	 */
	getActiveCapacities(items) { return items.filter(i => i.type === "capacity" && i.system.rank && !i.system.moduleData?.cota?.scalable) }
	/**
	 * @ override
	 */	
	canEquipItem(item, bypassChecks) {
		if (item?.system?.worn) return true;
 		if (super.canEquipItem(item, bypassChecks)) {
			const properties = item?.system?.moduleData?.cota?.item?.properties
			if (properties?.strMin) {
				if (this.system?.stats?.str?.value < properties?.strMinValue) {
					ui.notifications.warn(game.i18n.format("COF.notification.insufficientStrength", {actorName: this.name, itemName: item?.name}));
					return false;
				}
			}
		} else return false;
		return true;
	}
	/**
	 * @name checkItemsForInsufficientStrength
	 * @description vérifie que les armes dont la force minimum requise n'est pas respecté soit désactivées
	 * 
	 */
	checkItemsForInsufficientStrength() {
		const  items = this?.items.filter(i=> i.system.worn && i.system?.moduleData?.cota?.item?.properties.strMin && this.system?.stats?.str?.value < i.system?.moduleData?.cota?.item?.properties.strMinValue);
		items.forEach(item => {
			ui.notifications.warn(game.i18n.format("COF.notification.insufficientStrength", {actorName: this.name, itemName: item?.name}));
			this.toggleEquipItem(item, null);
		});
	}
	//#endregion

	//#region méthodes privées
	/**
	 * @name _getIncompetentItems
	 * @description Obtenir la liste des équipements non maîtrisées selon son type
	 * @private
	 * 
	 * @param {string} _type le type d'équipement
	 * 
	 * @returns {Array} la liste des équipements non maîtrisées ou une liste vide
	 */
	_getIncompetentItems(_type) {
		let items = this?.items;
		let profile = this.getProfile(items);
		return items.filter(item => item.system.subtype === _type && item.system.worn === true && !profile?.system?.moduleData?.cota?.martialTraining[item.system?.moduleData?.cota?.martialCategory]);
	}
	/**
	 * @name _getSpellItems
	 * @description Obtenir la liste des équipements de type sort
	 * @private
	 * 
	 * @returns {Array} la liste des équipements maîtrisées de type sort
	 */
	_getSpellItems() { return this?.items.filter(item => item?.system?.subtype === "spell" && item?.system?.worn === true); }
	//#endregion
}