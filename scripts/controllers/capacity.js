//#region imports
import { Capacity } from "../../../../systems/cof/module/controllers/capacity.js";
import { CotaActor } from "../actors/actor.js";
import { CotaItem } from "../items/item.js";
//#endregion

/**
 * @class CotaCapacity
 * @classdesc Classe contrôleur permettant la gestion des capacités évolutives et avancées
 * @extends {Capacity} hérite de la classe contrôleur Capacity du système COF
 * 
 * @method toggleCheck : surcharge permettant de prendre en compte la gestion des capacités évolutives et avancées
 * @method checkAdvancedCapacity : Vérifie et met à jour les capacités avancées
 * @method removeFromActor : Surcharge permettant de prendre en compte la suppression des capacités évolutives et la mise à jours des capacités avancées
 */
export class CotaCapacity extends Capacity {
	//#region méthodes publiques

	/**
	 * @name toggleCheck
	 * @description surcharge permettant de prendre en compte la gestion des capacités évolutives et avancées
	 * @public @static @override
	 * 
	 * @param {CotaActor} actor l'acteur
	 * @param {String} capId Id de la capacité
	 * @param {String} pathId Id de la voie
	 * @param {CotaItem} isUncheck la capacité est décochée
	 * 
	 * @returns l'acteur modifié
	 */
	static async toggleCheck(actor, capId, pathId, isUncheck) {
		await super.toggleCheck(actor, capId, pathId, isUncheck);
		const path = foundry.utils.duplicate(actor.items.get(pathId));
		const caste = actor.items.find(c => c.type === "capacity" && c.flags?.caste !== undefined)?.flags.caste;
		path?.system?.capacities.forEach(async (cap) => {
			let capacity = cap;
			if (isUncheck) {
				CotaCapacity.removeFromActor(actor, capacity);
			} else {
				capacity = actor.items.find(c_1 => c_1.system.key === cap.data.key);
				const linkedCapacity = (capacity) ? actor.items.find(c_2 => c_2.type === "capacity" && c_2.flags?.originKey === capacity.system.key) : null;
				if (capacity && !linkedCapacity) {
					const scalableCapacities = capacity.system.moduleData?.cota?.scalableCapacities;
					if (scalableCapacities) {
						let capacities = await CotaCapacity._getScalableCapacities(scalableCapacities.profilePaths, caste);
						if (capacities.length > 0) {
							let d = new Dialog({
								title: capacity.name + " [" + game.i18n.localize("COF.ui.rank") + " " + capacity.system.rank + "]",
								content: await CotaCapacity._ContentDialog(capacities),
								buttons: CotaCapacity._MakeButtons(actor, path, capacity, capacities),
								default: "1",
								close: () => {
									let isChecked = false;
									capacities.forEach(cap_2 => {
										let radio = d.element.find("#" + cap_2.system.key);
										if (!isChecked)
											isChecked = radio[0]?.checked;
									});
									if (!isChecked) {
										ui.notifications.warn(game.i18n.localize("COTA.ui.warn.selectCapacity"));
										d.render(true);
									}
								}
							}, {
								width: 420
							});
							d.render(true);
						}
					}
				}
				CotaCapacity.checkAdvancedCapacity(actor, capacity?.system?.key);
			}
		});
	}
	/**
	 * @name checkAdvancedCapacity
	 * @description Vérifie et met à jour les capacités avancées
	 *              Règle : Parfois, le personnage pourrait porter son choix sur une capacité issue d’une voie composant son profil. Il pourrait alors acquérir «deux fois» 
					cette capacité, une fois via sa voie culturelle et une fois via sa voie de base. La capacité gagne alors les effets avancés décrits à la suite des effets standards.
					Seules les capacités de rang 1 et 2 peuvent devenir des capacités avancées.
	 * @public @static 
	 * 
	 * @param {string} key la clé de la capacité
	 */
	static checkAdvancedCapacity(actor, key) {
		let capacities = actor.items.filter(c => c.type === "capacity" && c.system?.key === key && c.system?.moduleData.cota.advanced !== null);
		if (capacities) {
			let items = [];
			capacities.forEach(cap => {
				let system = cap.system;
				if (system.moduleData.cota.advanced !== null) system.moduleData.cota.advanced = (capacities.length > 1);
				items.push(cap.toObject(false));
			});
			if (items.length > 0) return actor.updateEmbeddedDocuments("Item", items);
		}
	}
	/**
	 * @name removeFromActor
	 * @description Surcharge permettant de prendre en compte la suppression des capacités évolutives et la mise à jours des capacités avancées
	 * @public @static @override
	 * 
	 * @param {*} actor l'acteur
	 * @param {*} capacity la capacité
	 */
	static async removeFromActor(actor, capacity) {
		let toRemove = null;
		capacity = capacity instanceof Array ? capacity[0] : capacity;
		if (!capacity.data.checked) toRemove = actor.items.find(c => c.type === "capacity" && c.flags?.originKey === capacity.data.key);
		if (toRemove) {
			await super.removeFromActor(actor, toRemove).then((cap) => {
				CotaCapacity.checkAdvancedCapacity(actor, cap[0].system.key); 
			});
		} else {
			CotaCapacity.checkAdvancedCapacity(actor, capacity?.data?.key);
		}
	}
	//#endregion

	//#region méthodes privées
	/**
	 * @name _getScalableCapacities
	 * @description Obtenir la liste des capacités évolutives
	 * @private @static @async
	 * 
	 * @param {Array} profilePaths les voies de profils
	 * @param {string} caste la caste
	 * 
	 * @returns {Array} la liste des capacités évolutives
	 */
	static async _getScalableCapacities(profilePaths, caste) {
		let capacities = new Array();
		if (profilePaths) {
			for (let i = 0; i < profilePaths.length; i++) {
				const profilePath = profilePaths[i];
				const path = await game.packs.get("cota.paths").getDocument(profilePath[0]);
				const rank = profilePath[2];
				const canBeAdvanced = profilePath[3];
				for (let y = 0; y < 2; y++) {
					let capacity = await game.packs.get("cota.capacities").getDocument(path?.system?.capacities[y]._id);
					capacity.name = path?.system?.capacities[y].name;
					capacity.system.rank = y + 1;
					capacity.flags.pathName = path.name;
					capacity.flags.caste = profilePath[1];
					if (!canBeAdvanced) capacity.system.moduleData.cota.advanced = null;
					switch (rank) {
						case "1": if (y === 0) capacities.push(capacity); break;
						case "2": if (y === 1) capacities.push(capacity); break;
						default: capacities.push(capacity);
					}
				}
			}
		}
		return (caste !== undefined) ? capacities.filter(c => c.flags.caste === caste) : capacities;
	}
	/**
	 * @name _CreateScalableCapacity
	 * @description Créer la nouvelle capacité évoluée issue du choix effectué sur le rang 2 ou 4 de la voie culturelle
	 *              Règle : En progressant, un personnage peut - notamment via les voies culturelles – faire l’acquisition de capacités issues d’autres voies que celles de son 
					profil. Si la capacité est évolutive en fonction du rang atteint dans une voie, la voie de référence qui détermine ce rang est toujours la voie ayant 
					permis l’acquisition de cette capacité.
	 * @private @static @async
	 * 
	 * @param {CotaActor} actor l'acteur
	 * @param {CotaItem} path la voie
	 * @param {CotaItem} capacity la capacité d'origine
	 * @param {CotaItem} scalableCapacity la capacité évolutive
	 * @param {number} rank le rang
	 */
	static async _CreateScalableCapacity(actor, path, capacity, scalableCapacity, rank) {
		let newCap = scalableCapacity;
		newCap.name = capacity.name + " [" + game.i18n.localize("COF.ui.rank") + " " + capacity.system.rank + "] - " + scalableCapacity.name;
		newCap.system.rank = rank;
		newCap.system.checked = true;
		newCap.flags.originKey = capacity.system.key;
		newCap.flags.core = { sourceId: capacity.flags.core.sourceId };
		newCap.system.moduleData.cota.scalable = true;
		newCap.system.moduleData.cota.scalableCapacities = null;
		newCap.system.species = actor.items.find(specie => specie.type === "species");
		let items = [];
		items.push(scalableCapacity.toObject(false));
		await actor.createEmbeddedDocuments("Item", items);
		CotaCapacity.checkAdvancedCapacity(actor, scalableCapacity.system.key);
	}
	/**
	 * @name _MakeButtons
	 * @description Construit les boutons de la fenêtre de dialogue
	 * @private @static
	 * 
	 * @param {CotaActor} actor l'acteur
	 * @param {CotaItem} path  la voie
	 * @param {CotaItem} capacity la capacité
	 * @param {Array} capacities la liste des capacités
	 * @returns les boutons
	 */
	static _MakeButtons(actor, path, capacity, capacities) {
		let buttons = new Array();
		let cancel = new Object();
		let submit = new Object();
		//
		cancel.icon = '<i class="fas fa-times"></i>'
		cancel.label = game.i18n.localize("COF.ui.cancel");
		cancel.callback = function () { }
		//
		submit.icon = '<i class="far fa-dot-circle"></i>'
		submit.label = game.i18n.localize("COTA.ui.selectCapacity");
		submit.callback = function (html) {
			capacities.forEach(cap => {
				let radio = html.find("#" + cap.system.key)
				if (radio && radio[0]?.checked) {
					CotaCapacity._CreateScalableCapacity(actor, path, capacity, cap, cap.system.rank);
				}
			});
		}
		buttons.push(submit);
		return buttons
	}
	/**
	 * @name _ContentDialog
	 * @description obtenir le contenu de la fenêtre de dialogue
	 * @private @static
	 * 
	 * @param {Array} capacities la liste des capacités
	 * 
	 * @see scalable-capacities-dialog.hbs
	 * @returns le contenu
	 */
	static async _ContentDialog(capacities) {
		const template = 'modules/cota/templates/dialogs/scalable-capacities-dialog.hbs';
		const content = await renderTemplate(template, { capacities: capacities });
		return content;
	}
	//#endregion
}