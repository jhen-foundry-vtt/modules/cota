//#region imports
import { Capacity } from "../../../../systems/cof/module/controllers/capacity.js";
import { Profile } from "../../../../systems/cof/module/controllers/profile.js";
import { CotaActor } from "../actors/actor.js";
//#endregion

/**
 * @class CotaProfile
 * @classdesc  Cette classe controleur permet de gérer les actions de CRUD sur les profils
 * 			      
 * @extends {Profile}
 * 
 * @method addMagicTalendToActor        : ajoute le talend magique pour les profils mystiques
 * @method removeMagicTalendFromActor   : supprime le talend magique de l'acteur pour les profils mystiques
 * @method getCapacityFromMagicTalent   : Obtenir la capacité du talent magique associée
 */
export class CotaProfile extends Profile {
	//#region méthodes publiques
	/**
	 * @name addMagicTalendToActor
	 * @description ajoute le talend magique pour les profils mystiques
	 * @public @static @async
	 * 
	 * @param {CotaActor} actor l'acteur
	 * @param {string} magicTalent le code du talent magique
	 * 
	 * @returns {Item} le talend magique crée
	 */
	static async addMagicTalendToActor(actor, magicTalent) {
		const capacity = await this.getCapacityFromMagicTalent(magicTalent);
		if (capacity) return Capacity.addCapacitiesToActor(actor, [capacity]);
	}
	/**
	 * @name removeMagicTalendFromActor
	 * @description supprime le talend magique de l'acteur pour les profils mystiques
	 * @public @static @async
	 * 
	 * @param {CotaActor} actor l'acteur
	 * @param {string} magicTalent le code du talent magique
	 */
	static async removeMagicTalendFromActor(actor, magicTalent) {
		const capacity = await this.getCapacityFromMagicTalent(magicTalent);
		const capacities = actor.items.filter(item => item.type === "capacity" && item.name === capacity?.name);
		if (capacities.length > 0) {
			Capacity.removeCapacitiesFromActor(actor, capacities).then(() => {
				ui.notifications.info(parseInt(capacities.length) + ((capacities.length > 1) ? " capacités ont été supprimées." : " capacité a été supprimée"));
			});
		}
	}
	/**
	 * @name getCapacityFromMagicTalent
	 * @description Obtenir la capacité du talent magique associée
	 * @public @static @async
	 * 
	 * @param {string} magicTalent le code du talent magique
	 * @returns {Item} la capacité 
	 */
	static async getCapacityFromMagicTalent(magicTalent) {
		if (magicTalent) {
			const caps = await game.packs.get("cota.capacities").getDocuments();
			for (const key in caps) {
				if (Object.hasOwnProperty.call(caps, key)) {
					const element = caps[key];
					if (element.name.slugify({ strict: true }) === game.i18n.localize("COTA.profile." + magicTalent).slugify({ strict: true })) {
						element.flags.core = { sourceId: "Compendium.cota.capacities." + element._id };
						return element;
					}
				}
			}
		}
	}
	//#endregion
}