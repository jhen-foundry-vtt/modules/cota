//#region imports
import { EntitySummary } from "../../../../systems/cof/module/controllers/entity-summary.js";
import { CofItem } from "../../../../systems/cof/module/items/item.js";
//#endregion

/**
 * 
 */
export class CotaItem extends CofItem {
	//#region méthodes publiques
	/**
	 * @nom initialize
	 * @description initialise une instance de la classe et ajoute l'objet cota avec les spécialisations par type
	 * @public
	 */
	initialize() {
		try {
			let cota = null;
			if (!this.system.moduleData) Object.assign(this.system, { "moduleData": {} });
			if (this.type === "profile" && !this.system.moduleData.cota) {
				cota = {
					"cota": {
						"family": "fighters",
						"bonuses": {
							"melee": null,
							"ranged": null,
							"fp": null,
							"magic": null
						},
						"martialTrainingMax": null,
						"martialTraining": {
							"farmerWeapons": true,
							"warWeapons": false,
							"heavyWarWeapons": false,
							"hastWeapons": false,
							"duelingWeapons": false,
							"throwingWeapons": false,
							"draughtWeapons": false,
							"shootingWeapons": false,
							"lightArmour": false,
							"heavyArmour": false
						},
						"privilegedSpecie": [],
						"magicTalent": null,
						"doubledMagicBurn": false
					}
				}
				Object.assign(this.system.moduleData, cota)
				this.changeFamily("1d10", 2, 2, 0, 0, "wis", "1", null, true, 3);
			} else if (this.type === "path" && !this.system.moduleData.cota) {
				cota = {
					"cota": {
						"properties": {
							"cultural": false
						},
						"family": null
					}
				}
				Object.assign(this.system.moduleData, cota)
			} else if (this.type === "item" && !this.system.moduleData.cota) {
				cota = {
					"cota": {
						"martialCategory": "farmer",
						"item": {
							"properties": {
								"strMin" : false
							}
						}
					}
				}
				Object.assign(this.system.moduleData, cota)                
			} else if (this.type === "capacity" && !this.system.moduleData.cota) {
				cota = {
					"cota": {
						"scalable": false,
						"advanced" : false,
						"scalableCapacities": {
							"profilePaths": []
						}
					}
				}
				Object.assign(this.system.moduleData, cota)                
			}
		}
		catch (err) {
			console.error(`${game.i18n.localize("ERREUR.objet.initialise")} ${this.constructor.name} ${this.id} :`);
			console.error(err);
		}
	}
	/**
	 * @name changeFamily
	 * @description
	 * @public
	 * 
	 * @param {*} dv les dés de vie
	 * @param {*} melee le bonus d'attaque de mélée
	 * @param {*} ranged le bonus d'attaque à distance
	 * @param {*} magic le bonus de magie
	 * @param {*} fp le bonus de points de chance
	 * @param {*} spellcasting 
	 * @param {*} mpfactor 
	 * @param {*} magicTalent 
	 * @param {*} doubleMagicBurn 
	 * @param {*} martialTrainingMax 
	 * 
	 * @returns 
	 */
	changeFamily(dv, melee, ranged, magic, fp, spellcasting, mpfactor, magicTalent, doubleMagicBurn, martialTrainingMax) {
		try {
			if (this.type !== "profile") throw new Error(game.i18n.localize("COTA.error.itemIsNotProfile"));
			let system = this.system;
			let cotaData = system.moduleData.cota;
			system.dv = dv;
			cotaData.bonuses.melee = melee;
			cotaData.bonuses.ranged = ranged;
			cotaData.bonuses.magic = magic;
			cotaData.bonuses.fp = fp;
			system.spellcasting = spellcasting;
			system.mpfactor = mpfactor;
			cotaData.magicTalent = magicTalent;
			cotaData.doubledMagicBurn = doubleMagicBurn;
			cotaData.martialTrainingMax = martialTrainingMax;
			for (const property in cotaData.martialTraining) cotaData.martialTraining[property] = (property === "farmerWeapons") ? true : false;
			return this.update({ "system": system });
		} catch (error) {
			ui.notifications.error(error);
		}
	}
	/**
	 * @nom checkProfileMartialTraining
	 * @description vérifie 
	 * @public
	 * 
	 * @param {*} martial 
	 * @param {*} checked 
	 * 
	 * @return
	 */
	checkProfileMartialTraining(martial, checked) {
		let max, i = 0;
		try {
			if (this.type !== "profile") throw new Error(game.i18n.localize("COTA.error.itemIsNotProfile"));
			let cotaData = this.system.moduleData.cota;
			max = cotaData.martialTrainingMax + 1;
			let dataMartial = cotaData.martialTraining;
			dataMartial[martial] = checked;
			if (checked) {
				for (const property in dataMartial) if (dataMartial[property]) i++;
				if (i > max && ((this.actor) ? this.actor?.system.level.value === 1 : true)) dataMartial[martial] = false;
			}
			this.update({ "system.moduleData.cota.martialTraining": dataMartial });
		} catch (error) {
			ui.notifications.error(error);
			return false;
		}
		if (i > max && ((this.actor) ? this.actor?.system.level.value === 1 : true)) throw new Error(game.i18n.localize("COTA.warn.martialTrainingIsComplete"));
	}
	/**
	 * @nom addSpecieToProfile
	 * @description Ajoute le peuple privilégié au profil
	 * @public
	 * 
	 * @param {Item} specieData le peuple
	 * @returns 
	 */
	addSpecieToProfile(specieData) {
		try { if (this.type !== "profile") throw new Error(game.i18n.localize("COTA.error.itemIsNotProfile")); } catch (error) { ui.notifications.error(error); }
		let specie = this.system.moduleData.cota.privilegedSpecie;
		if (specie && specie.length < 1) {
			specie.push(EntitySummary.create(specieData));
			return this.update({ "system.moduleData.cota.privilegedSpecie": specie });
		} else throw new Error(game.i18n.localize("COTA.warn.NotMoreThanOnePrivilegedSpecie"));
	}
	/**
	 * @nom deleteSpecieToProfile
	 * @description suppression du peuple privilégié du profil
	 * @public
	 * 
	 * @returns retourne la donnée modifiée
	 */
	deleteSpecieToProfile() {
		try {
			if (this.type !== "profile") throw new Error(game.i18n.localize("COTA.error.itemIsNotProfile"));
			let specie = this.system.moduleData.cota.privilegedSpecie;
			if (specie && specie.length === 1) {
				specie.pop();
				return this.update({ "system.moduleData.cota.privilegedSpecie": specie });
			}
		} catch (error) { ui.notifications.error(error); }
	}

	getMartialCategory() {
		if (!this.system.properties?.weapon && !this.system.properties?.protection) return;
		return this.system.moduleData?.cota?.martialCategory;;
	}
	//#endregion
}
