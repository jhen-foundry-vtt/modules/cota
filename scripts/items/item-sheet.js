//#region imports
import { CofItemSheet } from "../../../../systems/cof/module/items/item-sheet.js";
import { Repeater, Repeaters } from "../system/CRepeaters.js";
//#endregion
/**
 * 
 */
export class CotaItemSheet extends CofItemSheet {
	//#region propriétés
	/** 
	 * répétiteur sur les voies de profil pour la notion des capacités évolutives
	 */
	#repeaters = new Repeaters(new Repeater("profilePath", null, "system.moduleData.cota.scalableCapacities.profilePaths", ["", "", "0", true]));

	/** @override */
	static get defaultOptions() {
		return foundry.utils.mergeObject(super.defaultOptions, {
			classes: ["cof", "cota", "sheet", "item", this.type],
			width: 600,
			height: 600,
			tabs: [{ navSelector: ".sheet-navigation", contentSelector: ".sheet-body", initial: "description" }],
			dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }]
		});
	}
	/**
	 * @override
	 */
	get template() { return "modules/cota/templates/items/item-sheet.hbs"; }
	//#endregion

	//#region Méthodes publiques
	/**
	* @override
	*/
	getPackPrefix() { return "cota"; }
	/**
	 * @override
	 */
	getPathRoot() { return "modules/cota"; }

	/**
	 * @name getData
	 * @description obtenir les données de l'Item
	 * @public @override
	 * @param {*} options 
	 * @returns les données
	 */
	async getData(options) {
		if (!this.item?.system?.moduleData ||
			!this.item?.system?.moduleData?.cota) this.item?.initialize()
		const context = await super.getData(options);
		context.configCota = game.cota.config;
		return context;
	}
	/**
	 * @name activateListeners
	 * @description Gère les abonnements aux évènements.
	 * 
	 * @param html document html
	 */
	activateListeners(html) {
		super.activateListeners(html);
		html.find('.change-family').change(ev => {
			ev.preventDefault();
			let li = $(ev.currentTarget), family = li.get(0).value;
			switch (family) {
				case "fighters": this.item?.changeFamily("1d10", 2, 2, 0, 0, "wis", "1", null, true, 3);
					break;
				case "adventurers": this.item?.changeFamily("1d8", 1, 1, 0, 2, "wis", "1", null, false, 2);
					break;
				case "mystics": this.item?.changeFamily("1d6", 0, 0, 2, 0, "wis", "2", null, false, 1);
					break;
				default: this.item?.changeFamily("", 0, 0, 0, 0, "", "0", null, false, 0);
			}
		});
		html.find('.click-martial').click(ev => {
			ev.preventDefault();
			let li = $(ev.currentTarget);
			try {
				this.item?.checkProfileMartialTraining(li.data("martial"), li.get(0).checked);
			} catch (error) {
				if (error.message === game.i18n.localize("COTA.warn.martialTrainingIsComplete")) ui.notifications.warn(error.message);
			}
		});
		html.find('.item-delete-specie').click(ev => {
			ev.preventDefault();
			this.item?.deleteSpecieToProfile();
		});
		/** */
		html.find(".repeater-bouton").click(this._onRepeaters.bind(this));
	}
	//#endregion

	//#region Méthodes privées
	/**
	 * @name _onDropItem
	 * @description 
	 * 
	 * @protected @override
	 * @param {*} event l'évènement
	 * @param {*} data  la donnée
	 */
	_onDropItem(event, data) {
		super._onDropItem(event, data);
		Item.fromDropData(data).then(item => {
			const itemData = foundry.utils.duplicate(item);
			try {
				if (itemData.type === "species") return this._onDropSpecieItem(event, itemData);
			} catch (error) {
				if (error.message === game.i18n.localize("COTA.warn.notMoreThanOnePrivilegedSpecie")) ui.notifications.warn(error.message);
			}
		});
	}
	/**
	 * @name _onDropSpecieItem
	 * @description
	 * 
	 * @private
	 * @param {Event} event 
	 * @param {Item} itemData les données de l'item
	 * @returns l'item modifié ou null
	 */
	_onDropSpecieItem = function (event, itemData) {
		event.preventDefault();
		if (this.item?.type === "profile") return this.item?.addSpecieToProfile(itemData);
		else return null;
	}
	/** @override  */
	_updateObject = (event, formData) => {
		// update de la liste des répétiteurs 
		if (this.item?.type === "capacity") this.#repeaters.forEach(repeater => { repeater.update(formData); });
		super._updateObject(event, formData);
	}
	/**
	 * @name _onRepeaters
	 * @description : Gestion des données de la liste des répétiteurs selon ses actions Ajout et Suppression
	 * @private
	 * 
	 * @param {Event} event -> L'événement initial
	 * 
	 */
	_onRepeaters = (event) => {
		if (this.item?.type === "capacity") {
			this.#repeaters[0].data = this.item.system.moduleData.cota.scalableCapacities.profilePaths;
			this.#repeaters.onRepetearsActions(this, event);
		}
	}
	//#endregion
}
