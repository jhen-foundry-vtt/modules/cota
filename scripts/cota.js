//#region imports
import { COTA } from "./system/config.js"
import { CotaActor } from "./actors/actor.js";
import { CotaItem } from "./items/item.js";
import { CofActorSheet } from "../../../systems/cof/module/actors/actor-sheet.js";
import { CotaActorSheet } from "./actors/actor-sheet.js";
import { CofItemSheet } from "../../../systems/cof/module/items/item-sheet.js";
import { CotaItemSheet } from "./items/item-sheet.js";
import { CofLootSheet } from "../../../systems/cof/module/actors/loot-sheet.js";
import { CotaLootSheet } from "./actors/loot-sheet.js";
import { preloadHandlebarsTemplates } from "./system/templates.js";
import { registerHandlebarsHelpers } from "./system/helpers.js";
import { registerSystemSettings } from "./system/settings.js";
//#endregion

//#region Hooks
/**
 * @name init
 * @description initialisation du module cota
 */
Hooks.once("init", () => {
	console.info("COTA | Module COTA loading...");

	// Define custom Entity classes
	CONFIG.Actor.documentClass = CotaActor;
	CONFIG.Item.documentClass = CotaItem;

	//Create a namespace cota
	game.cota = {
		config: COTA
	};

	// Unregister core and cod sheet application classes
	Actors.unregisterSheet("core", ActorSheet);    
	Actors.unregisterSheet("cof", CofActorSheet);
	Actors.unregisterSheet("cof", CofLootSheet);

	Items.unregisterSheet("core", ItemSheet);
	Items.unregisterSheet("cof", CofItemSheet);
	
	// Register sheet application classes
	Actors.registerSheet("cota", CotaActorSheet, { types: ["character", "npc", "encounter"], makeDefault: true, label: "COF.sheet.actor" });
	Actors.registerSheet("cota", CotaLootSheet, { types: ["loot"], makeDefault: true, label: "COF.sheet.loot" });
	Items.registerSheet("cota", CotaItemSheet, { types: ["item", "capacity", "profile", "path", "species"], makeDefault: true, label: "COF.sheet.item" });

	// Preload Handlebars Templates
	preloadHandlebarsTemplates();

	// Register Handlebars helpers
	registerHandlebarsHelpers();

	// Register System Settings
	registerSystemSettings();
});

/**
 * @name welcomeMessage
 * @description Message d'accueil à l'activation du module
 */
async function welcomeMessage() {
	ChatMessage.create({
	  type: CONST.CHAT_MESSAGE_TYPES.OTHER,
	  content: "L'activation du module <b>Terres d'Arran</b> a activé les options suivantes :<ul><li>Points de récupération</li><li>Points de chance</li><li>Points de Mana</li><li>PJ Incompétent</li><li>Encombrement</ul>",
	  speaker: { alias: "Terres d'Arran" }
	})
	game.user.setFlag("cota", "welcomeMessageShown", true)
  }

/**
 * @name ready
 * @description le système est chargé
 */
Hooks.once("ready", async () => {   
	console.info("COTA | Module COTA loaded.");
	COTA.paths = await game.packs.get("cota.paths").getDocuments().then(paths => { return paths.filter(path => path.system.properties.profile); });
	const settings = {
		useRecovery: true,
		useFortune: true,
		useMana: true,
		useIncompetentPJ: true,
		useOverload: true
	}

	// Active les options obligatoires pour COTA
	game.settings.settings.forEach(async setting => {
		if (setting.namespace === 'cof' && typeof settings[setting.key] !== 'undefined' && game.settings.get('cof', setting.key) !== settings[setting.key]) {
		  await game.settings.set('cof', setting.key, settings[setting.key])
		}
	})

	// message d'accueil à l'activation du module
	if (!game.user.getFlag("cota", "welcomeMessageShown")) {
		welcomeMessage()
	}
});
//#endregion